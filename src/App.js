import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Input from '@material-ui/core/Input';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image'; 
import LeakAdd from '@material-ui/icons/LeakAdd';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import Dialpad from '@material-ui/icons/Dialpad';
import AccountCircle from '@material-ui/icons/AccountCircle';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import Paper from '@material-ui/core/Paper';

import { chats, messages} from './mock-data';

const styles = theme => ({
  root: {
    display: 'flex',
    overflow: `auto`,
    height: `78%`,
    position: `relative`,
  },
  appBar: {
    width: `calc(100% - ${340}px)`,
    marginLeft: 340,
  },
  drawer: {
    width: 340,
    flexShrink: 0,
    height: `64px`,
  },
  drawerPaper: {
    width: 340,
  },
  toolbar: theme.mixins.toolbar,
  content: {
    minHeight: `100vh`,
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    position: `relative`,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  inputMassege: {
    position: `absolute`,
    bottom:`2%`,
    width: `90%`,
    boxShadow: `0 0 15px rgba(0,0,0,0.5)`,
  }, 
  input: {
    margin: theme.spacing.unit,
    width: `90%`,
    
  },
  chatSwitch: {
    position: `absolute`,
    width: `100%`,
    bottom:`0%`,  
    height: `9%`,
    },
  seach: {
    width: `100%`,
    height: `9%`,    
    },
  myChat: {
    width: `50%`,
    height: `100%`,
    float: `left`,
    boxShadow: `inset 0 0 15px rgba(0,0,0,0.5)`,
    },
  explore: {
    width: `50%`,
    height: `100%`,
    float: `left`,
    boxShadow: `inset 0 0 15px rgba(0,0,0,0.5)`,
    },
  dialpad: {
    float: `left`,
    paddingLeft: `10px`,
  }, 
  accountcircle: {
    position: `absolute`,
    right: `3%`,
  },
  topMenu: {
    width: `auto`,
  },
  typographyh1: {
    float: `left`,
  },
});

function PermanentDrawerLeft(props) {
  
  const { classes } = props;

  return (
    <div className={classes.root}>
      <CssBaseline />
      
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <div className={classes.accountcircle}>
            <button>
             <AccountCircle />
            </button>
           </div>
           <div classNmae={classes.topMenu}>
            <div className={classes.typographyh1}>
              <Typography variant="h6" color="inherit" noWrap>
                DogeCodes React
              </Typography>
            </div>
            <div className={classes.dialpad}>
              <button>
                <Dialpad />
              </button>
            </div>
          </div>
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <div className={classes.seach}>
          <Input
            placeholder="Seach chat..."
            className={classes.input}
            inputProps={{
              'aria-label': 'Description',
            }}
          />
        </div> 
       <Divider />
        <div className={classes.root}>
          <List>
            <ListItem>
              <Avatar className={classes.avatar}>
                GR
              </Avatar>
              <ListItemText primary="Photos" secondary="Jan 9, 2014" />
            </ListItem>
 
          </List>
        </div>
        <Divider />
          <div className={classes.chatSwitch}>
            <button className={classes.myChat}>
              <LeakAdd />
              <br/>My chats
            </button>
            <button className={classes.explore}>
              <LibraryBooks />
              <br/>Explore
            </button>
          </div>
      </Drawer> 
      
      <main className={classes.content}>
        <div className={classes.toolbar} />
          <Typography >
            <div>
              <div className={classes.root}>
                <List>
                  <ListItem>
                    <Avatar >
                      DD
                    </Avatar>
                    <ListItemText primary="Photos" secondary="Jan 9, 2014" />
                  </ListItem>
                 </List>
              </div>
            </div>
          </Typography>
            <div className={classes.inputMassege}>
              <Paper className={classes.root} elevation={1}>
                <Input
                  placeholder="Type you message..."
                  className={classes.input}
                  inputProps={{
                    'aria-label': 'Description',
                  }}
                />
            </Paper>
          </div>
      </main>
    </div>
  );
}

PermanentDrawerLeft.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PermanentDrawerLeft);